import pytest

def add(x, y):
    """ Ajoute deux nombres"""
    return x+y

def test_add():
    assert(add(0.1, 0.2) == pytest.approx(0.3) )

def inc(x):
    """ incrémente un nombre de 1"""
    return x + 1

def test_a():
    assert inc(3) == 4

def dec(x):
    return x - 1

